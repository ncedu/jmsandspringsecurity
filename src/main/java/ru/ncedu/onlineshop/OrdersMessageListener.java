package ru.ncedu.onlineshop;

/**
 * Created by Никита on 09.09.14.
 */

import org.slf4j.LoggerFactory;

import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.ArrayList;
import java.util.List;

/**
* Class handles incoming messages
*
* @see //PointOfIssueMessageEvent
*/
public class OrdersMessageListener implements MessageListener
{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(OrdersMessageListener.class);

    private static List<Message> messages = new ArrayList<Message>();

    public List<Message> getMessages() {
        return messages;
    }

    /**
     * Method implements JMS onMessage and acts as the entry
     * point for messages consumed by Springs DefaultMessageListenerContainer.
     * When DefaultMessageListenerContainer picks a message from the queue it
     * invokes this method with the message payload.
     */
    public void onMessage(Message message)
    {
        logger.info("Start. Messages in list: " + messages.size());
        logger.debug("Start. Incoming message: " + message + ". Messages list: " + messages);

        messages.add(message);

        logger.info("Finish");
        logger.debug("Finish.Message " + message + " had been added to the list: " + messages.size());
    }
}

package ru.ncedu.onlineshop.entity.users;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Created by Никита on 27.09.14.
 */

@Entity
@Table(name = "Authorities")
public class Authority implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "authority_id")
    int id;

    String authority;

    @ManyToOne(targetEntity = User.class,
               cascade = CascadeType.ALL,
               fetch =FetchType.EAGER,
               optional = true)
    @JoinColumn(name = "user_id",
                referencedColumnName = "user_id",
                nullable = false,
                table = "user_authorities")
    User user;

    public Authority() {}

    public Authority(String authority) {
        this.authority = authority;
    }

    public void setAuthority(String grantedAuthority) {
        this.authority = grantedAuthority;
    }

    /**
     * If the <code>GrantedAuthority</code> can be represented as a <code>String</code> and that
     * <code>String</code> is sufficient in precision to be relied upon for an access control decision by an {@link
     * org.springframework.security.access.AccessDecisionManager} (or delegate), this method should return such a <code>String</code>.
     * <p/>
     * If the <code>GrantedAuthority</code> cannot be expressed with sufficient precision as a <code>String</code>,
     * <code>null</code> should be returned. Returning <code>null</code> will require an
     * <code>AccessDecisionManager</code> (or delegate) to specifically support the <code>GrantedAuthority</code>
     * implementation, so returning <code>null</code> should be avoided unless actually required.
     *
     * @return a representation of the granted authority (or <code>null</code> if the granted authority cannot be
     * expressed as a <code>String</code> with sufficient precision).
     */
    @Override
    public String getAuthority() {
        return authority;
    }
}

package ru.ncedu.onlineshop.entity.users;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Никита on 27.09.14.
 */
@Entity
@Table(name = "audit")
public class AuditItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "audit_item_id")
    private  Long id;

    Date time;
    String message;

    @ManyToOne(targetEntity = User.class,
               cascade = CascadeType.ALL,
               fetch = FetchType.LAZY,
               optional = true)
    @JoinColumn(name = "user_id",
                referencedColumnName = "user_id",
                nullable = false,
                table = "user_audit")
    User user;
}

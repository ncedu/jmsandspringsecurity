package ru.ncedu.onlineshop;

/**
 * Created by Никита on 09.09.14.
 */

import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.JMSException;
import javax.jms.Queue;

public class OrdersMessageSender
{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(OrdersMessageSender.class);

    private JmsTemplate jmsTemplate;
    private Queue ordersQueue;

    /**
     * Sends message using JMS Template.
     *
     * @param message the message
     * @throws JMSException the jMS exception
     */
    public void sendMessage(String message) throws JMSException
    {
        logger.info("Start. Sending message: " + message);
        logger.debug("Start. Sending message. Queue[" + ordersQueue.toString() + "] Message[" + message + "]");
        jmsTemplate.convertAndSend(ordersQueue, message);
        logger.info("Finish.");
    }

    public void setJmsTemplate(JmsTemplate template)
    {
        this.jmsTemplate = template;
    }

    public void setOrdersQueue(Queue queue)
    {
        this.ordersQueue = queue;
    }
}
package ru.ncedu.onlineshop.vaadin;

import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;

/**
 * Created by Никита on 24.09.14.
 */
public class HeadLoginComponent extends HorizontalLayout implements Button.ClickListener {

    private VerticalLayout loginForm = null;
    private VerticalLayout logoutForm = null;
    private VerticalLayout signUpForm = null;
    private Button signInButton = new Button("Sign In", this);
    private Button signUpButton = new Button("Sign Up", this);
    private Button logoutButton = new Button("Logout", this);
    private TextField loginField = new TextField("Login:", "");
    private PasswordField passwordField = new PasswordField("Password:", "");
    private PasswordField repeatPasswordField = new PasswordField("Repeat password:", "");
    private TextField firstNameField = new TextField("First name:");
    private TextField secondNameField = new TextField("Second name:");

    // Points
    boolean signingUp = false;

    private UserDetails authenticate(String login, String password) {
        ServletContext servletContext = VaadinServlet.getCurrent().getServletContext();
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

        //CustomUserDetailsServise users = (CustomUserDetailsServise)context.getBean("customUserDetailsServise");
        ProviderManager authenticationManager = (ProviderManager)context.getBean("authenticationManager");

        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(login, password);

        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (AuthenticationException e) {
            Notification.show("Bad authentication!", Notification.Type.HUMANIZED_MESSAGE);
        }

        return (authentication != null ? (UserDetails)authentication.getPrincipal() : null);
    }

    private void createLoginForm() {
        loginForm = new VerticalLayout();
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.addComponent(signInButton);
        buttons.addComponent(signUpButton);
        loginForm.addComponent(loginField);
        loginForm.addComponent(passwordField);
        loginForm.addComponent(buttons);
    }

    private void createLogoutForm() {
        logoutForm = new VerticalLayout();
        logoutForm.addComponent(logoutButton);
    }

    private void createSignUpForm() {
        signUpForm = new VerticalLayout();
        signUpForm.addComponent(loginField);
        signUpForm.addComponent(passwordField);
        signUpForm.addComponent(repeatPasswordField);
        signUpForm.addComponent(firstNameField);
        signUpForm.addComponent(secondNameField);
        signUpForm.addComponent(signUpButton);
    }

    public HeadLoginComponent() {
        addComponent(new Label("->Shop"));
        if (authenticated()) {
            createLogoutForm();
            addComponent(logoutForm);
        } else {
            createLoginForm();
            addComponent(loginForm);
        }
    }

    private boolean authenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            return true;
        }
        return false;
    }

    /**
     * Called when a {@link com.vaadin.ui.Button} has been clicked. A reference to the
     * button is given by {@link com.vaadin.ui.Button.ClickEvent#getButton()}.
     *
     * @param event An event containing information about the click.
     */
    @Override
    public void buttonClick(Button.ClickEvent event) {
        if (event.getButton() == logoutButton) {
            SecurityContextHolder.clearContext();
            removeComponent(logoutForm);
            if (loginForm == null) {
                createLoginForm();
            }
            addComponent(loginForm);
        } else if (event.getButton() == signInButton) {
            authenticate(loginField.getValue(), passwordField.getValue());
            if (authenticated()) {
                removeComponent(loginForm);
                if (logoutForm == null) {
                    createLogoutForm();
                }
                addComponent(logoutForm);
            }
        } else if (event.getButton() == signUpButton) {
            if (signingUp) {
                if (loginField.getValue().equals(""))
                if (!passwordField.getValue().equals(repeatPasswordField.getValue())) {
                    Notification.show("Password and repeated one aren't the same");
                } else {
                    // TODO: try to create new user
                    authenticate(loginField.getValue(), passwordField.getValue());
                    if (authenticated()) {
                        signingUp = false;
                        removeComponent(signUpForm);
                        if (logoutForm == null) {
                            createLogoutForm();
                        }
                        addComponent(logoutForm);
                    }
                }
            } else {
                removeComponent(loginForm);
                if (signUpForm == null) {
                    createSignUpForm();
                }
                addComponent(signUpForm);
                Notification.show("Input, please, registration information", Notification.Type.HUMANIZED_MESSAGE);
                signingUp = true;
            }
        }
    }
}

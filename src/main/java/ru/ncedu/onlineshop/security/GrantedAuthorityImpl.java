package ru.ncedu.onlineshop.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by Никита on 17.09.14.
 */
public class GrantedAuthorityImpl implements GrantedAuthority {
    String authority;

    public enum Authorities {ROLE_ANONYMOUS, ROLE_USER, ROLE_ADMIN;}

    public GrantedAuthorityImpl(Authorities authority) {
        switch (authority) {
            case ROLE_ANONYMOUS: this.authority = "ROLE_ANONYMOUS";
            case ROLE_USER: this.authority = "ROLE_USER";
            case ROLE_ADMIN: this.authority = "ROLE_ADMIN";
        }
    }

    @Override
    public String getAuthority() {
        return authority;
    }
}

package ru.ncedu.onlineshop.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Никита on 17.09.14.
 */
public class UsersManager {
    Map<String, User> users;
    public UsersManager() {
        users = new HashMap<String, User>();
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new GrantedAuthorityImpl(GrantedAuthorityImpl.Authorities.ROLE_ADMIN));
        authorities.add(new GrantedAuthorityImpl(GrantedAuthorityImpl.Authorities.ROLE_USER));
        users.put("a", new User("a", "ap", authorities));
        authorities.clear();
        authorities.add(new GrantedAuthorityImpl(GrantedAuthorityImpl.Authorities.ROLE_USER));
        users.put("u", new User("u", "up", authorities));

    }
    //TODO: dummy. Include db
    public UserDetails getUser(String username) throws UsernameNotFoundException {
        if (!users.containsKey(username)) {
            throw new UsernameNotFoundException("Such user doesn't exist. Username: " + username);
        }
        return users.get(username);
    }
}
